﻿using System;
using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;
using System.Globalization;
using ProgressBar;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class GameController : MonoBehaviour
{
	public GameObject[] Hazards;

    public GameObject RepearPack;
    public GameObject EnergyPack;
    public GameObject BombPack;

    public Vector3 SpawnValues;
	public int HazardCount;
	public float SpawnWait;
	public float StartWait;
	public float WaveWait;

    public float RepairChance;
    public float EnergyChance;
    public float BombChance;

    public Text ScoreText;
    public Text ScoreAdded;
    public Text DamageAdded;
    public Text HealAdded;
    public Text EnergyAdded;
    public Text RestartText;
	public Text GameOverText;
    public Text Speed;
    public Text Wave;

    private bool _gameOver;
	private bool _restart;
	private int _score;
    private ProgressRadialBehaviour _progressHealth;
    private ProgressRadialBehaviour _progressEnergy;
    private ProgressRadialBehaviour _progressSpeed;
    private PlayerController _playerController;
    private int _wave;

    void Start ()
	{
		_gameOver = false;
		_restart = false;
		RestartText.text = "";
		GameOverText.text = "";
		_score = 0;
		UpdateScore ();
		StartCoroutine (SpawnWaves ());

	    ProgressRadialBehaviour[] findObj = FindObjectsOfType<ProgressRadialBehaviour>();

	    _progressHealth = findObj[2];

	    foreach (ProgressRadialBehaviour progress in findObj)
	    {
	        switch (progress.name)
	        {
                case "ProgressHealth":
                    _progressHealth = progress;
                    break;
	            case "ProgressEnergy":
	                _progressEnergy = progress;
                    break;
	            case "ProgressSpeed":
	                _progressSpeed = progress;
                    break;
            }
	    }

	    _progressHealth.Value = 100;
	    _progressEnergy.Value = 100;
	    _progressSpeed.Value = 100;

        _playerController = FindObjectOfType<PlayerController>();
    }
	
	void Update ()
	{
		if (_restart)
		{
			if (Input.GetButton("Fire1"))
			{
                SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
			}
		}

	}

    void FixedUpdate()
    {
        MakeGui();
        FixFunction();
    }

    void FixFunction()
    {
        AsteroidController[] enemies = FindObjectsOfType<AsteroidController>();

        foreach (AsteroidController enemy in enemies)
        {
            if (!enemy.enabled)
                enemy.Set_health(0, false);
        }
    }

    IEnumerator SpawnWaves()
    {
        yield return new WaitForSeconds(StartWait);
        while (true)
        {
            if (_gameOver)
            {
                RestartText.text = "Press 'Fire' for Restart";
                _restart = true;
                break;
            }
            _wave++;
            for (int i = 0; i < HazardCount; i++)
            {
                GameObject hazard = Hazards[Random.Range(0, Hazards.Length)];
                Vector3 spawnPosition = new Vector3(SpawnValues.x, Random.Range(-SpawnValues.y, SpawnValues.y),
                    SpawnValues.z);
                Quaternion spawnRotation = Quaternion.identity;
                Mover asteroidMover = hazard.GetComponent<Mover>();
                AsteroidController asteroidController = hazard.GetComponent<AsteroidController>();
                if (asteroidController.RandomSpawn)
                    asteroidController.SpawnTimes = Random.Range(0, 3);
                asteroidMover.MoveDirection = Mover.Direction.Left;
                Instantiate(hazard, spawnPosition, spawnRotation);
                yield return new WaitForSeconds(SpawnWait);
            }

            MoreDificult();

            yield return new WaitForSeconds(WaveWait);

        }
    }

    void MoreDificult()
    {
        _playerController.Movement.Speed += _playerController.Movement.SelfAcc;
        HazardCount++;
        SpawnWait -= 0.1f;
    }

    void MakeGui()
    {
        _progressHealth.Value = _playerController.Health.Value / _playerController.Health.Max * 100;
        _progressEnergy.Value = _playerController.Energy.Value / _playerController.Energy.Max * 100;
        _progressSpeed.Value = _playerController.Movement.Speed / _playerController.Movement.Max * 100;
        Speed.text = Mathf.Round(_playerController.Movement.Speed * 10).ToString(CultureInfo.CurrentCulture);
        Wave.text = _wave.ToString();
    }

    public void SpawnOnDestroy(GameObject expAsteroid)
    {
        AsteroidController asteroidController = expAsteroid.GetComponent<AsteroidController>();
        if (asteroidController.SpawnTimes > 0)
        {
            asteroidController.Health /= 2;
            asteroidController.ScoreValue /= 2;
            asteroidController.Protected = 2;
            asteroidController.SpawnTimes--;

            float scale = expAsteroid.transform.localScale.x;

            expAsteroid.transform.localScale = new Vector3(expAsteroid.transform.localScale.x / 2,
                expAsteroid.transform.localScale.y / 2,
                expAsteroid.transform.localScale.z / 2);

            Quaternion spawnRotation = Quaternion.identity;
            Mover asteroidMover = expAsteroid.GetComponent<Mover>();
            asteroidMover.Speed *= 2;

            asteroidMover.MoveDirection = Mover.Direction.BottomLeft;
            Instantiate(expAsteroid, expAsteroid.transform.position + transform.right * scale, spawnRotation);

            asteroidMover.MoveDirection = Mover.Direction.TopLeft;
            Instantiate(expAsteroid, expAsteroid.transform.position, spawnRotation);
        }


        float repair = Random.Range(0, 100);
        if (repair < RepairChance)
        {
            Quaternion spawnRotationRepair = Quaternion.identity;
            Instantiate(RepearPack, expAsteroid.transform.position, spawnRotationRepair);
            return;
        }

        float energy = Random.Range(0, 100);
        if (energy < EnergyChance)
        {
            Quaternion spawnRotationRepair = Quaternion.identity;
            Instantiate(EnergyPack, expAsteroid.transform.position, spawnRotationRepair);
            return;
        }

        float bomb = Random.Range(0, 100);
        if (bomb < BombChance)
        {
            Quaternion spawnRotationRepair = Quaternion.identity;
            Instantiate(BombPack, expAsteroid.transform.position, spawnRotationRepair);
            return;
        }

    }

    public void AddScore (int newScoreValue, GameObject o = null)
	{
	    if (!_gameOver)
	    {
	        _score += newScoreValue;
	        AddScoreAnimate(newScoreValue, o);
	        UpdateScore();
        }

	}

    public void AddScoreAnimate(float newScoreValue, GameObject o)
    {
        if (o != null)
        {
            Canvas canvas = FindObjectOfType<Canvas>();
            Text score = Instantiate(ScoreAdded); //
            score.text = "+" + newScoreValue;
            score.transform.localPosition = o.transform.localPosition;
            score.transform.parent = canvas.transform;
            score.transform.localScale = ScoreAdded.transform.localScale;
            score.gameObject.SetActive(true);
            Destroy(score.gameObject, 0.6f);
        }
    }

    public void AddDamageAnimate(float newValue, GameObject o)
    {
        if (o != null)
        {
            Canvas canvas = FindObjectOfType<Canvas>();
            Text score = Instantiate(DamageAdded); 
            score.text = "-" + Math.Round(newValue);
            score.transform.localPosition = o.transform.localPosition + Vector3.up;
            score.transform.parent = canvas.transform;
            score.transform.localScale = DamageAdded.transform.localScale;
            score.gameObject.SetActive(true);
            Destroy(score.gameObject, 0.6f);
        }
    }

    public void AddHealAnimate(float newValue, GameObject o)
    {
        if (o != null)
        {
            Canvas canvas = FindObjectOfType<Canvas>();
            Text score = Instantiate(HealAdded);
            score.text = "+" + Math.Round(newValue);
            score.transform.localPosition = o.transform.localPosition + Vector3.up;
            score.transform.parent = canvas.transform;
            score.transform.localScale = HealAdded.transform.localScale;
            score.gameObject.SetActive(true);
            Destroy(score.gameObject, 0.6f);
        }
    }

    public void AddEnergyAnimate(float newValue, GameObject o)
    {
        if (o != null)
        {
            Canvas canvas = FindObjectOfType<Canvas>();
            Text score = Instantiate(EnergyAdded); 
            score.text = "+" + newValue;
            score.transform.localPosition = o.transform.localPosition + Vector3.up;
            score.transform.parent = canvas.transform;
            score.transform.localScale = EnergyAdded.transform.localScale;
            score.gameObject.SetActive(true);
            Destroy(score.gameObject, 0.6f);
        }
    }

    void UpdateScore ()
	{
		ScoreText.text = _score.ToString();
	}
	
	public void GameOver ()
	{
		GameOverText.text = "Game Over";
		_gameOver = true;
	}
}