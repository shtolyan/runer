﻿using UnityEngine;

namespace Assets.Scripts.WeaponControl
{
    public abstract class WeaponRoot : MonoBehaviour
    {
        public float EnergyCosts;
        public float Damage;
        public float FireRate;
        public float Speed;
        public GameObject Shot;
        public AudioClip ShotSound;
        public AudioSource Audio;
        private readonly PlayerController _playerController;
        private float _nextFire = 0;

        public void Fire()
        {
            if (Time.time > _nextFire && _playerController.Energy.Value > EnergyCosts)
            {
                _nextFire = Time.time + FireRate;
                GameObject shot = Instantiate(Shot, _playerController.ShotSpawn.position, _playerController.ShotSpawn.rotation);
                ShotMover mover = shot.GetComponent<ShotMover>();
                mover.Damage = Damage;
                Audio.Play();
                _playerController.Energy.Value -= EnergyCosts;
            }
        }

        protected WeaponRoot()
        {
            _playerController = FindObjectOfType<PlayerController>();
            Audio = _playerController.GetComponent<AudioSource>();
        }

        public void Init()
        {
            ShotMover shotMover = Shot.GetComponent<ShotMover>();
            shotMover.Speed = Speed;
            shotMover.Damage = Damage;
            Audio.clip = ShotSound;
        }
      
    }

    public enum Weapon
    {
        Laser, DoubleLaser, FireBall, Rocket
    }
}
