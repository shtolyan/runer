﻿using Assets.Scripts.Data.Weapons;
using System;
using System.Linq;
using UnityEngine;

namespace Assets.Scripts.WeaponControl
{
    class WeaponControl : MonoBehaviour
    {
        private Weapon _playerWeapon = Weapon.Laser;
        private readonly PlayerController _playerController;

        //---------
        private readonly Laser _laser;
        private readonly DoubleLaser _doubleLaser;
        private readonly FireBall _fireBall;
        private readonly Rocket _rocket;

        //----------

        public WeaponControl()
        {
            _laser = new Laser();
            _doubleLaser = new DoubleLaser();
            _fireBall = new FireBall();
            _rocket = new Rocket();
            _playerController = FindObjectOfType<PlayerController>();
        }

        public void Fire()
        {
            switch (_playerWeapon)
            {
                case Weapon.Laser:
                    _laser.Fire();
                    break;
                case Weapon.DoubleLaser:
                    _doubleLaser.Fire();
                    break;
                case Weapon.FireBall:
                    _fireBall.Fire();
                    break;
                case Weapon.Rocket:
                    _rocket.Fire();
                    break;
            }
        }

        public void NextWeapon()
        {
            Weapon newWeapon = _playerWeapon;

            if (newWeapon < Enum.GetValues(typeof(Weapon)).Cast<Weapon>().Last())
                newWeapon++;
            else newWeapon = Enum.GetValues(typeof(Weapon)).Cast<Weapon>().First();

            SetWeapon(newWeapon);
        }

        public void PreviosWeapon()
        {
            Weapon newWeapon = _playerWeapon;

            if (newWeapon > Enum.GetValues(typeof(Weapon)).Cast<Weapon>().First())
                newWeapon--;
            else newWeapon = Enum.GetValues(typeof(Weapon)).Cast<Weapon>().Last();

            SetWeapon(newWeapon);
        }

        public void SetWeapon(Weapon newWeapon)
        {
            if (_playerWeapon == newWeapon)
                return;

            _playerWeapon = newWeapon;

            switch (_playerWeapon)
            {
                case Weapon.Laser:
                    _laser.Init();
                    break;
                case Weapon.DoubleLaser:
                    _doubleLaser.Init();
                    break;
                case Weapon.FireBall:
                    _fireBall.Init();
                    break;
                case Weapon.Rocket:
                    _rocket.Init();
                    break;
            }

            _playerController.Weapon = _playerWeapon;
        }

    }
}
