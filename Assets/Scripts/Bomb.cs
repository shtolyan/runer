﻿using System.Collections;
using UnityEngine;

class Bomb : MonoBehaviour
{
    public GameObject Explosion;
    public int ExplotionTimes = 5;
    public float ExplotionWhait = 0.5f;
    private float _lifeTime = 15;

    void Update()
    {
        _lifeTime -= Time.deltaTime;

        if (_lifeTime <= 0)
        {
            MakeExplosion(gameObject.transform);
            Destroy(gameObject);
        }
    }

    void OnTriggerEnter(Collider other)
    {

        if (other.tag == "Player")
        {
            StartCoroutine(MakeExplotions());
            MakeExplosion(other.transform);
            Destroy(gameObject);
        }

    }

    void MakeExplosionAllEnemies()
    {
        AsteroidController[] enemies = FindObjectsOfType<AsteroidController>();

        foreach (AsteroidController enemy in enemies)
        {
            enemy.Set_health(0, true);
        }
    }

    void MakeExplosion(Transform transform)
    {
        if (Explosion != null)
        {
            GameObject explosion = Instantiate(Explosion, transform.position, transform.rotation);
            Destroy(explosion, 2f);
        }
    }

    IEnumerator MakeExplotions()
    {
        while (true)
        {
            if (ExplotionTimes <= 0)
                break;

            MakeExplosionAllEnemies();
            ExplotionTimes--;
            yield return new 
                WaitForSeconds(ExplotionWhait);
        }
    }
}

