﻿using System;
using UnityEngine;
using System.Collections;

public class DestroyByBoundary : MonoBehaviour
{
	void OnTriggerExit (Collider other)
	{
	    try
	    {
	        Destroy(other.transform.parent.gameObject);
        }
	    catch
	    {
	        Destroy(other.gameObject);
        }
        
	}
}