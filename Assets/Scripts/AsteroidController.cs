﻿using System;
using UnityEngine;
using System.Collections;

public class AsteroidController : MonoBehaviour
{
	public GameObject Explosion;
	public GameObject PlayerExplosion;
	public int ScoreValue;
    public float Health = 5;
    public float SpawnTimes = 2;
    public float Protected;
    public float LifeTime = 15;
    public float Damage = 1;
    public float ExplotionTime = 1;

    private GameController _gameController;
    public bool RandomSpawn;

    private float Get_health()
    {
        return Health;
    }

    public void Set_health(float value, bool score)
    {
        Health = value;
        GetDamage(score);
    }

    void Update()
    {
        Protected -= Time.deltaTime;
        ExplotionTime += Time.deltaTime;
        if (Protected <= -LifeTime)
            Destroy(gameObject);
    }

    void Start ()
    {
        Protected = 1;
        GameObject gameControllerObject = GameObject.FindGameObjectWithTag ("GameController");
		if (gameControllerObject != null)
		{
			_gameController = gameControllerObject.GetComponent <GameController>();
		}
		if (_gameController == null)
		{
			Debug.Log ("Cannot find 'GameController' script");
		}
	}

    void OnTriggerEnter(Collider other)
    {

        if (other.tag == "Enemy")
        {
            if (Protected <= 0)
            {
                Mover myMovermover = gameObject.GetComponent<Mover>();
                Mover otherMover = other.gameObject.GetComponentInParent<Mover>();
                AsteroidController asteroidController = other.gameObject.GetComponentInParent<AsteroidController>();
                if (asteroidController != null)
                {
                    float damage = asteroidController.Damage;

                    if (myMovermover.MoveDirection == Mover.Direction.Left)
                        myMovermover.MoveDirection = Mover.Direction.BottomLeft;

                    if (otherMover.MoveDirection == Mover.Direction.Left)
                        otherMover.MoveDirection = Mover.Direction.TopLeft;

                    MakeExplosion(other.transform);

                    Set_health(Get_health() - damage, false);
                    asteroidController.Set_health(asteroidController.Get_health() - Damage, false);

                    return;
                }
            }
            else return;
        }

        if (other.tag == "Bolt")
        {
            ShotMover shot = other.GetComponent<ShotMover>();
            Set_health(Get_health() - shot.Damage, true);
            //_gameController.AddDamageAnimate(shot.Damage, gameObject);
        }

        if (other.tag == "Player")
        {
            PlayerController controller = other.gameObject.GetComponent<PlayerController>();
            Mover mover = gameObject.GetComponent<Mover>();
            float damage = Damage * (mover.Speed + controller.Movement.Speed +
                                     gameObject.transform.localScale.magnitude * 10);
            controller.Set_damage_health(damage);
            MakeExplosion(other.transform);
            Set_health(0, false);
            return;
        }

        if (other.tag == "Boundary")
        {
            return;
        }

        MakeExplosion(other.transform);
        Destroy(other.gameObject);
       
    }

    void MakeExplosion(Transform transform)
    {
        if (Explosion != null)
        {
            if (ExplotionTime > 0.1f)
            {
                GameObject explosion = Instantiate(Explosion, transform.position, transform.rotation);
                Destroy(explosion, 2f);
                ExplotionTime = 0;
            }
        }
    }

    void GetDamage(bool score)
    {
        if (Health <= 0)
        {
            try
            {
                MakeExplosion(transform);
                if (score)
                {
                    _gameController.AddScore(ScoreValue, gameObject);
                    _gameController.SpawnOnDestroy(gameObject);
                }
                SpawnTimes--;
                Destroy(gameObject);
            }
            catch (Exception e)
            {
                Debug.Log(e);
                Destroy(gameObject);
            }
            
        }
    }
}