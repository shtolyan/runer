﻿using System;
using UnityEngine;
using System.Collections;
using System.Linq;
using Assets.Scripts.Data.Weapons;
using Assets.Scripts.WeaponControl;
using ProgressBar;

[System.Serializable]
public class Boundary 
{
	public float xMin, xMax, yMin, yMax;
}

[System.Serializable]
public class Movement
{
    public float Speed, Acceleration, Breaking, Deceleration, TurnSpeed, SelfAcc, Min, Max; 
}

[System.Serializable]
public class Health
{
    public float Value, Defence, Max, Protected;
}

[System.Serializable]
public class Energy
{
    public float Value, Max, RestoreSpeed;
}

public class PlayerController : MonoBehaviour
{
	public float Tilt;
    public Health Health;
    public Energy Energy;
    public float Damage = 5;
    public float ProtectedTime = 0.5f;
    public Boundary Boundary;
    public Movement Movement;
    public GameObject Explosion;
	public Transform ShotSpawn;
    public Weapon Weapon = Weapon.Laser;

    private GameController _gameController;
    private WeaponControl _weaponControl;
    

    public float Get_health()
    {
        return Health.Value;
    }

    public void Set_damage_health(float value)
    {
        if (Health.Protected <= 0)
        {
            Health.Value -= value - value * Health.Defence;
            UpdateHealth();
            _gameController.AddDamageAnimate(value, gameObject);
            Health.Protected = ProtectedTime;
        }     
    }

    public void Set_heal_health(float value)
    {
        if (Health.Protected <= 0)
        {
            Health.Value += value;
            UpdateHealth();
            _gameController.AddHealAnimate(value, gameObject);
            Health.Protected = ProtectedTime;
        }
    }

    public void GiveEnergy(float value, bool animate = true)
    {
        if (Energy.Value + value <= Energy.Max)
            Energy.Value += value;
        else
            Energy.Value = Energy.Max;
        
        if (animate)
            _gameController.AddEnergyAnimate(value, gameObject);

    }

    private void UpdateHealth()
    {
        if (Health.Value <= 0)
        {
            MakeExplosion(transform);
            _gameController.GameOver();
            Destroy(gameObject);
        }

        if (Health.Value > Health.Max)
            Health.Value = Health.Max;
    }

    void MakeExplosion(Transform transform)
    {
        if (Explosion != null)
        {
            GameObject explosion = Instantiate(Explosion, transform.position, transform.rotation);
            Destroy(explosion, 2f);
        }
    }

    void Start()
    {
        GameObject gameControllerObject = GameObject.FindGameObjectWithTag("GameController");
        if (gameControllerObject != null)
        {
            _gameController = gameControllerObject.GetComponent<GameController>();
        }
        if (_gameController == null)
        {
            Debug.Log("Cannot find 'GameController' script");
        }

        _weaponControl = new WeaponControl();

    }

    void Update()
    {

        if (Input.GetButton("Fire1"))
        {
            _weaponControl.Fire();

        }

        if (Input.GetButton("Fire2"))
        {
            _weaponControl.NextWeapon();
        }


        if (Movement.Speed > Movement.Max)
            Movement.Speed = Movement.Max;

        if (Movement.Speed < Movement.Min)
            Movement.Speed = Movement.Min;

        _gameController.AddScore(Convert.ToInt32(Movement.Speed / 10));

        Health.Protected -= Time.deltaTime;
        GiveEnergy(Energy.RestoreSpeed, false);
        _weaponControl.SetWeapon(Weapon);
    }


    void FixedUpdate ()
	{
		float moveHorizontal = Input.GetAxis ("Horizontal");
		float moveVertical = Input.GetAxis ("Vertical");

		Vector3 movement = new Vector3 (moveHorizontal, moveVertical, 0.0f);
		GetComponent<Rigidbody>().velocity = movement * Movement.TurnSpeed;

        GetComponent<Rigidbody>().position = new Vector3
        (
            Mathf.Clamp(GetComponent<Rigidbody>().position.x, Boundary.xMin, Boundary.xMax),
            Mathf.Clamp(GetComponent<Rigidbody>().position.y, Boundary.yMin, Boundary.yMax),
            0.0f
            
        );

	    GetComponent<Rigidbody>().rotation = 
            Quaternion.Euler(0f, -270f, -90 - GetComponent<Rigidbody>().velocity.y * -Tilt);

    }
    
    
}


