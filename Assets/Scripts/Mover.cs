﻿using UnityEngine;
using System.Collections;

public class Mover : MonoBehaviour
{
	public float Speed;
    public Direction MoveDirection = Direction.Left;

    private PlayerController _controller;
    private float _playerSpeed;

	void Start ()
	{
        _controller = FindObjectOfType<PlayerController>();
        _playerSpeed = 0;
    }

    private void Update()
    {
        if (_controller != null)
            _playerSpeed = _controller.Movement.Speed;
        
        switch (MoveDirection)
        {
            case Direction.Left:
                gameObject.transform.position = new Vector3
                    (
                        gameObject.transform.position.x - Speed / 100 - _playerSpeed / 50,
                        gameObject.transform.position.y,
                        gameObject.transform.position.z
                    );
                break;
            case Direction.BottomLeft:
                gameObject.transform.position = new Vector3
                    (
                        gameObject.transform.position.x - Speed / 100 - _playerSpeed / 50,
                        gameObject.transform.position.y - Speed / 160,
                        gameObject.transform.position.z
                    );
                break;
            case Direction.TopLeft:
                gameObject.transform.position = new Vector3
                    (
                        gameObject.transform.position.x - Speed / 100 - _playerSpeed / 100,
                        gameObject.transform.position.y + Speed / 160, 
                        gameObject.transform.position.z
                    );
                break;
        }
    }

    public enum Direction
    {
        Left,
        TopLeft,
        BottomLeft
    }
}
