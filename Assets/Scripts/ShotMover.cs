﻿using UnityEngine;
using System.Collections;

public class ShotMover : MonoBehaviour
{
    public float Speed;
    public float Damage;

    void Start()
    {
        GetComponent<Rigidbody>().velocity = transform.forward * Speed;
    }
}
