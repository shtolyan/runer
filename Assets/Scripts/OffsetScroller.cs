﻿using UnityEngine;
using System.Collections;

public class OffsetScroller : MonoBehaviour
{
    public float ScrollSpeed;
    private Vector2 _savedOffset;
    private Renderer _renderer;
    private PlayerController _controller;
    private float _metrs;

    void Start()
    {
        _controller = FindObjectOfType<PlayerController>();
        _renderer = gameObject.GetComponent<Renderer>();
        _savedOffset = _renderer.sharedMaterial.GetTextureOffset("_MainTex");
    }

    void Update()
    {
        float acceleration = _controller.Movement.Speed / 100;
        _metrs += ScrollSpeed * acceleration / 5;
        Vector2 offset = new Vector2(_savedOffset.x, -_metrs);
        _renderer.sharedMaterial.SetTextureOffset("_MainTex", offset);
    }

    void OnDisable()
    {
        _renderer.sharedMaterial.SetTextureOffset("_MainTex", _savedOffset);
    }
}