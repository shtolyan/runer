﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Assets.Scripts.Data.Weapons
{
    public class Laser : WeaponControl.WeaponRoot
    {

        public Laser()
        {
            EnergyCosts = 1;
            Damage = 1;
            FireRate = 0.15f;
            Speed = 10;
            Shot = Resources.Load(@"Shot\Bolt") as GameObject;
            ShotSound = Resources.Load(@"Audio\Bolt") as AudioClip;

            Init();
        }

    }
}
