﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Assets.Scripts.Data.Weapons
{
    public class Rocket : WeaponControl.WeaponRoot
    {

        public Rocket()
        {
            EnergyCosts = 20;
            Damage = 15;
            FireRate = 0.9f;
            Speed = 6;
            Shot = Resources.Load(@"Shot\Rocket") as GameObject;
            ShotSound = Resources.Load(@"Audio\Rocket") as AudioClip;

            Init();
        }

    }
}
