﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Assets.Scripts.Data.Weapons
{
    public class FireBall : WeaponControl.WeaponRoot
    {

        public FireBall()
        {
            EnergyCosts = 0.5f;
            Damage = 0.15f;
            FireRate = 0.001f;
            Speed = 6;
            Shot = Resources.Load(@"Shot\FireBall") as GameObject;
            ShotSound = Resources.Load(@"Audio\FireBall") as AudioClip;

            Init();
        }

    }
}
