﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Scripts.Data.Weapons
{
    public class DoubleLaser : WeaponControl.WeaponRoot
    {
        public DoubleLaser()
        {
            EnergyCosts = 1.2f;
            Damage = 3f;
            FireRate = 0.1f;
            Speed = 15;
            Shot = Resources.Load(@"Shot\BoltDouble") as GameObject;
            ShotSound = Resources.Load(@"Audio\BoltDouble") as AudioClip;

            Init();
        }

        

    }
}
