﻿using UnityEngine;

class RepairPack : MonoBehaviour
{
    public float AddHealth = 150;
    public GameObject Explosion;
    private float _lifeTime = 15;

    void Update()
    {
        _lifeTime -= Time.deltaTime;

        if (_lifeTime <= 0)
        {
            MakeExplosion(gameObject.transform);
            Destroy(gameObject);
        }
    }

    void OnTriggerEnter(Collider other)
    {

        if (other.tag == "Player")
        {
            PlayerController controller = other.gameObject.GetComponent<PlayerController>();
            controller.Set_heal_health(AddHealth);
            MakeExplosion(other.transform);
            Destroy(gameObject);
        }

    }

    void MakeExplosion(Transform transform)
    {
        if (Explosion != null)
        {
            GameObject explosion = Instantiate(Explosion, transform.position, transform.rotation);
            Destroy(explosion, 2f);
        }
    }
}

